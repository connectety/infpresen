package example.java;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Employee {
	private String empNo;
	private String firstName;
	private String lastName;
	private String email;
	private String position;
	
	private boolean active;
	
	public Employee(String empNo, String email, String firstName, String lastName, String position, boolean active) {
		this.empNo = empNo;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.position = position;
		this.active = active;
	}
	
	public static ObservableList<Employee> getEmployees() {
		Employee section = new Employee("00", "Abc@gmail.com", "Section", "Housekeeping", "Section", false);
		Employee smith = new Employee("01", "Smith@gmail.com", "Susan", "Smith", "Salesman", true);
		Employee mcneil = new Employee("02", "McNeil@gmail.com", "Anne", "McNeil", "Cleck", false);
		
		return FXCollections.observableArrayList(section, smith, mcneil);
	}
	
	public String getEmpNo() {
		return empNo;
	}
	
	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getPosition() {
		return position;
	}
	
	public void setPosition(String position) {
		this.position = position;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
}