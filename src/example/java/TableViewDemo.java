package example.java;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class TableViewDemo extends Application {
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage stage) {
		TableView<Employee> table = new TableView<>();
		
		// Create column UserName (Data type of String).
		TableColumn<Employee, String> empNoCol = new TableColumn<>("No");
		
		// Create column Email (Data type of String).
		TableColumn<Employee, String> emailCol = new TableColumn<>("Email");
		
		// Create column FullName (Data type of String).
		TableColumn<Employee, String> fullNameCol = new TableColumn<>("Full Name");
		
		// Create 2 sub column for FullName.
		TableColumn<Employee, String> firstNameCol = new TableColumn<>("First Name");
		TableColumn<Employee, String> lastNameCol = new TableColumn<>("Last Name");
		
		// Add sub columns to the FullName
		fullNameCol.getColumns().addAll(firstNameCol, lastNameCol);
		
		// Active Column
		TableColumn<Employee, Boolean> activeCol = new TableColumn<>("Active");
		
		// Defines how to fill data for each cell.
		// Get value from property of UserAccount.
		empNoCol.setCellValueFactory(new PropertyValueFactory<>("empNo"));
		firstNameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
		lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
		activeCol.setCellValueFactory(new PropertyValueFactory<>("active"));
		
		// Display row data
		ObservableList<Employee> list = Employee.getEmployees();
		table.setItems(list);
		
		table.getColumns().addAll(empNoCol, emailCol, fullNameCol, activeCol);
		
		StackPane root = new StackPane();
		root.setPadding(new Insets(5));
		root.getChildren().add(table);
		
		stage.setTitle("TableView");
		
		Scene scene = new Scene(root, 450, 300);
		stage.setScene(scene);
		stage.show();
	}
	
	
}