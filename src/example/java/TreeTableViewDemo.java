package example.java;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class TreeTableViewDemo extends Application {
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage stage) {
		TreeTableView<Employee> treeTableView = new TreeTableView<>();
		
		// Create column FullName.
		TreeTableColumn<Employee, String> fullNameCol = new TreeTableColumn<>("Full Name");
		
		// Create 2 sub column for FullNameColumn.
		TreeTableColumn<Employee, String> firstNameCol = new TreeTableColumn<>("First Name");
		TreeTableColumn<Employee, String> lastNameCol = new TreeTableColumn<>("Last Name");
		
		// Add sub columns to the FullNameColumn
		fullNameCol.getColumns().addAll(firstNameCol, lastNameCol);
		
		// Create other Column
		TreeTableColumn<Employee, String> empNoCol = new TreeTableColumn<>("No. ");
		TreeTableColumn<Employee, String> genderCol = new TreeTableColumn<>("Gender");
		TreeTableColumn<Employee, String> positionCol = new TreeTableColumn<>("Position");
		TreeTableColumn<Employee, Boolean> activeCol = new TreeTableColumn<>("Active");
		
		// Defines how to fill data for each cell.
		// Get value from property of Employee.
		empNoCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("empNo"));
		firstNameCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("firstName"));
		lastNameCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("lastName"));
		positionCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("position"));
		activeCol.setCellValueFactory(new TreeItemPropertyValueFactory<>("active"));
		
		// Add columns to TreeTable.
		treeTableView.getColumns().addAll(empNoCol, fullNameCol, positionCol, genderCol, activeCol);
		
		// Data
		ObservableList<Employee> list = Employee.getEmployees();
		TreeItem<Employee> itemRoot = new TreeItem<>(list.get(0));
		TreeItem<Employee> itemSmith = new TreeItem<>(list.get(1));
		TreeItem<Employee> itemMcNeil = new TreeItem<>(list.get(2));
		
		// set root item
		itemRoot.getChildren().addAll(itemSmith, itemMcNeil);
		treeTableView.setRoot(itemRoot);
		
		StackPane root = new StackPane();
		root.setPadding(new Insets(5));
		root.getChildren().add(treeTableView);
		
		stage.setTitle("TreeTableView");
		
		Scene scene = new Scene(root, 450, 300);
		stage.setScene(scene);
		stage.show();
	}
}