package ui;

import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

public class SlideController {
	@FXML
	public GridPane grid;
	@FXML
	public ImageView img;
	
	@FXML
	public void initialize() {
		img.setPreserveRatio(true);
		img.fitWidthProperty().bind(grid.widthProperty());
		img.fitHeightProperty().bind(grid.heightProperty());
	}
}
