package ui;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;

public class Presentation extends Application {
	private static Scene mainScene;
	private static int slideNumber = 0;
	private static Parent[] slides;
	private static boolean covered = false;
	private static FlowPane blackSlide = new FlowPane();
	private Stage primaryStage;
	
	public static void main(String[] args) {
		blackSlide.setStyle("-fx-background-color: #0f0f0f");
		
		launch(args);
	}
	
	private ImageView getSnapshot(Parent slide) {
		// get width and height of window
		int width = (int) primaryStage.getScene().getWidth();
		int height = (int) primaryStage.getScene().getHeight();
		
		// Create images of the current slide
		WritableImage currentWI = new WritableImage(width, height);
		Image currentImg = slide.snapshot(new SnapshotParameters(), currentWI);
		return new ImageView(currentImg);
	}
	
	private void slideAnimation(Parent newRoot, Direction direct, boolean backwards) {
		// get width and height of window
		int width = (int) primaryStage.getWidth();
		int height = (int) primaryStage.getHeight();
		
		Parent currentSlide = primaryStage.getScene().getRoot();
		
		// Create images of the current slide
		ImageView currentImgView = getSnapshot(currentSlide);
		
		// set scene content to new slide for to scale the slide and create an image of it
		primaryStage.getScene().setRoot(newRoot);
		ImageView nextImgView = getSnapshot(newRoot);
		primaryStage.getScene().setRoot(currentSlide);
		
		StackPane transitionPane;
		KeyValue kv;
		if (backwards) {
			transitionPane = new StackPane(nextImgView, currentImgView);
			
			// create animation position which end outside of the slide
			switch (direct) {
				case UP:
					kv = new KeyValue(currentImgView.translateYProperty(), -height, Interpolator.EASE_BOTH);
					break;
				case RIGHT:
					kv = new KeyValue(currentImgView.translateXProperty(), width, Interpolator.EASE_BOTH);
					break;
				case DOWN:
					kv = new KeyValue(currentImgView.translateYProperty(), height, Interpolator.EASE_BOTH);
					break;
				default:
					kv = new KeyValue(currentImgView.translateXProperty(), -width, Interpolator.EASE_BOTH);
					break;
			}
		} else {
			transitionPane = new StackPane(currentImgView, nextImgView);
			
			// create animation position which end on the slide
			// and set the new slides position to outside of the slide
			switch (direct) {
				case UP:
					nextImgView.setTranslateY(-height);
					kv = new KeyValue(nextImgView.translateYProperty(), 0, Interpolator.EASE_BOTH);
					break;
				case RIGHT:
					nextImgView.setTranslateX(width);
					kv = new KeyValue(nextImgView.translateXProperty(), 0, Interpolator.EASE_BOTH);
					break;
				case DOWN:
					nextImgView.setTranslateY(height);
					kv = new KeyValue(nextImgView.translateYProperty(), 0, Interpolator.EASE_BOTH);
					break;
				default:
					nextImgView.setTranslateX(-width);
					kv = new KeyValue(nextImgView.translateXProperty(), 0, Interpolator.EASE_BOTH);
					break;
			}
		}

		// set background black, to handle background while transitioning
		transitionPane.setStyle("-fx-background-color: black");
		
		// set scene to transitionPane
		primaryStage.getScene().setRoot(transitionPane);
		
		// create slide animation
		Timeline timeline = new Timeline();
		timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(0.5), kv));
		// set scene to the new scene after animation
		timeline.setOnFinished(t -> primaryStage.getScene().setRoot(newRoot));
		timeline.play();
	}
	
	private void addSlideHandler() {
		mainScene.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.RIGHT || e.getCode() == KeyCode.SPACE || e.getCode() == KeyCode.PAGE_DOWN) {
				nextSlide();
			} else if (e.getCode() == KeyCode.LEFT || e.getCode() == KeyCode.PAGE_UP) {
				previousSlide();
			} else if (e.getCode() == KeyCode.B) {
				toggleBlackSlide();
			} else if (e.getCode() == KeyCode.F11) {
				this.primaryStage.setFullScreen(!this.primaryStage.isFullScreen());
			}
		});
		
		mainScene.setOnMousePressed(e -> {
			if (e.isPrimaryButtonDown()) {
				nextSlide();
			}
		});
	}
	
	private void toggleBlackSlide() {
		if (covered) {
			slideAnimation(slides[slideNumber], Direction.UP, true);
		} else {
			slideAnimation(blackSlide, Direction.UP, false);
		}
		
		covered = !covered;
	}
	
	private void previousSlide() {
		if (slideNumber > 0) {
			slideAnimation(slides[slideNumber - 1], Direction.LEFT, false);
			slideNumber--;
			
			covered = false;
		}
	}
	
	private void nextSlide() {
		if (slideNumber < slides.length - 1) {
			slideAnimation(slides[slideNumber + 1], Direction.RIGHT, false);
			slideNumber++;
			
			covered = false;
		}
	}
	
	private Parent[] getSlides(String[] slidePaths) throws IOException {
		Parent[] slides = new Parent[slidePaths.length];
		
		for (int i = 0; i < slidePaths.length; i++) {
			slides[i] = FXMLLoader.load(getClass().getResource(slidePaths[i]));
		}
		
		return slides;
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;
		this.primaryStage.setFullScreen(true);
		
		String[] slidePaths = new String[]{
				"res/view/slide1.fxml", "res/view/slide2.fxml", "res/view/slide3.fxml",
				"res/view/slide4.fxml", "res/view/slide5.fxml", "res/view/slide6.fxml",
				"res/view/slide7.fxml", "res/view/slide8.fxml", "res/view/slide9.fxml",
				"res/view/slide10.fxml", "res/view/slide11.fxml"
		};
		slides = getSlides(slidePaths);
		
		mainScene = new Scene(slides[0]);
		this.primaryStage.setScene(mainScene);
		
		addSlideHandler();
		
		this.primaryStage.show();
	}
	
	enum Direction {UP, RIGHT, DOWN, LEFT}
}
